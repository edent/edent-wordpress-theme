<div id="comments" class="comments-area">
<?php if ( $comments ) : ?>
	<h2 class="comment-reply-title">
		<?php
			printf( // WPCS: XSS OK.
				esc_html( 
					_nx( 
						'One thought on &ldquo;%2$s&rdquo;', 
						'%1$s thoughts on &ldquo;%2$s&rdquo;', 
						get_comments_number(), 
						'comments title', 
						'edent_simple' ) 
					),
				number_format_i18n( get_comments_number() ),
				'<span>' . get_the_title() . '</span>'
			);
		?>
	</h2>
	<ol class="comment-list">
		<?php
			wp_list_comments( array(
				'style'      => 'ol',
				'short_ping' => true,
			) );
		?>
	</ol>
<?php endif;

if ( comments_open() || pings_open() ) :
	comment_form();
elseif ( $comments ) : ?>
	<div id="respond">
		<p class="closed"><?php _e( 'Comments closed', 'edent' ); ?></p>
	</div>

<?php endif; ?>
</div>
